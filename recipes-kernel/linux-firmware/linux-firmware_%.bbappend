SRC_URI:append:aquila-am69 = " https://docs.toradex.com/116005-sx-pceax-board-files_v1.0.zip;name=sx-pceax-board-files"
SRC_URI[sx-pceax-board-files.sha256sum] = "f94f6382ac1673b84a17e424b5bff681a108eea869544d9e3a3d804f498ae28f"

ATH11K_FIRMWARE_PATH = "${nonarch_base_libdir}/firmware/ath11k/WCN6855/hw2.1"

do_install:append:aquila-am69 () {
	install -m 0644 ${UNPACKDIR}/sx-pceax-board-files/board-2_US_EU_JP.bin ${D}${ATH11K_FIRMWARE_PATH}
	install -m 0644 ${UNPACKDIR}/sx-pceax-board-files/board-2_UK_CA.bin    ${D}${ATH11K_FIRMWARE_PATH}

	rm -f ${D}${ATH11K_FIRMWARE_PATH}/board-2.bin
	ln -frs ${D}${ATH11K_FIRMWARE_PATH}/board-2_US_EU_JP.bin ${D}${ATH11K_FIRMWARE_PATH}/board-2.bin
}
